//2
db.fruits.aggregate([
		{
			$match: { onSale: true }
		},
		{
			$count: "fruitsOnSale"
		}
	])

//3
db.fruits.aggregate([
		{
			$match: { stock: {$gte: 20} }
		},
		{
			$count: "enoughStock"
		}
	])

//4
db.fruits.aggregate([
		{
			$match: { onSale: true }
		},
		{
			$group: { _id: null, avg_pice: { $avg: "$price" } }
		}
	])

//5
db.fruits.aggregate([
		{
			$match: { onSale: true }
		},
		{
			$group: { _id: null, max_price: { $max: "$price" } }
		}
	])

//6
db.fruits.aggregate([
		{
			$match: { onSale: true }
		},
		{
			$group: { _id: null, min_price: { $min: "$price" } }
		}
	])


